FROM python:3

ADD server.py /
ADD deploy.prototxt.txt /
ADD res10_300x300_ssd_iter_140000.caffemodel /


RUN pip install requests
RUN pip install opencv-python
RUN pip install numpy
RUN pip install Flask

CMD [ "python", "./server.py" ]