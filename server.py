from flask import Flask, request, Response
import numpy as np
import cv2

# Initialize the Flask application
app = Flask(__name__)

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe("deploy.prototxt.txt", "res10_300x300_ssd_iter_140000.caffemodel")

# route http posts to this method
@app.route('/api/blur_face', methods=['POST'])
def blur_face():
    r = request
    # convert string of image data to uint8
    nparr = np.frombuffer(r.data, np.uint8)
    # decode image
    image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    
    
    confidence_val = 0.5

    
    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0,
                                 (300, 300), (104.0, 177.0, 123.0))

    # pass the blob through the network and obtain the detections and
    # predictions
    print("[INFO] computing object detections...")
    net.setInput(blob)
    detections = net.forward()

    # loop over the detections
    for i in range(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]

        if confidence > confidence_val:
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            y = startY
            h2 = endY - startY
            x = startX
            w2 = endX - startX
            image[y:y + h2, x:x + w2] = cv2.blur(image[y:y + h2, x:x + w2], (43, 43))

    
    
    img_str = cv2.imencode('.jpg', image)[1].tostring()

    return Response(response=img_str, status=200, mimetype="application/json")


# start flask app
app.run(host="0.0.0.0", port=5000)