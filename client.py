from __future__ import print_function
import requests
import cv2
import numpy as np

def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized


addr = 'http://173.193.99.138:5000'
url = addr + '/api/blur_face'
headers = {'content-type': 'image/jpeg'}

def post_image(img_file):
    """ post image and return the response """
    # img = open(img_file, 'rb').read()
    img = cv2.imread(img_file, cv2.IMREAD_COLOR)
    img = image_resize(img, height = 1300)
    img_str = cv2.imencode('.jpg', img)[1].tostring()
    response = requests.post(url, data=img_str, headers=headers)
    return response


response = post_image('img1.jpg')

nparr = np.frombuffer(response.content, np.uint8)
image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

image = image_resize(image, height = 900)

cv2.imshow("Output", image)
cv2.waitKey(0)
